import java.awt.*;
import java.util.List;
import java.util.Random;

public class Flood {

    private int width;
    private int height;
    private int[] colors;
    private List<Color> availableColors;

    public Flood(int width, int height, List<Color> availableColors) {
        this.width = width;
        this.height = height;
        this.availableColors = availableColors;

        colors = new int[width * height];
        restart();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void changeColor(Color targetColor) {
        var colorIndex = availableColors.indexOf(targetColor);
        if (colorIndex != -1) {
            var x = 0;
            var y = height - 1;
            FloodFill floodFill = new FloodFill(colors, width, height, x,y, colorIndex);

            floodFill.start();
        }
    }

    public List<Color> getAvailableColors() {
        return availableColors;
    }

    public Color getColor(int x, int y) {
        return availableColors.get(colors[x + y * width]);
    }

    public void restart() {

        Random r = new Random();
        for (int i = 0; i < colors.length; i++) {
            colors[i] = r.nextInt(availableColors.size());
        }

    }

    public boolean isFinished() {
        var startColor = colors[0];
        for (int i = 0; i < colors.length; i++) {
            if (colors[i] != startColor) return false;
        }
        return true;
    }
}
