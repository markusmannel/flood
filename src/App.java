import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class App {
    private Flood flood;
    private Container container;

    private int tileWidth = 16;
    private int tileHeight = 16;

    private List<FloodButton> buttons = new ArrayList<>();

    public App(Flood flood, Container container) {
        this.flood = flood;
        this.container = container;
        resize();
    }


    public void resize() {
        tileWidth = container.getWidth() / flood.getWidth();
        // +1 to have a bottom row for selecting the tile!
        tileHeight = (int) (container.getHeight() / (flood.getHeight() + 4));
        buttons.clear();
        var gameHeight = tileHeight * flood.getHeight();

        var buttonAreaHeight = container.getHeight() - gameHeight;

        int numButtons = flood.getAvailableColors().size();
        var spaceBetween = 20;
        var marginSide = 20;
        int remainingContainerWidth = container.getWidth() - marginSide - spaceBetween * (numButtons - 1);
        var buttonWidth = remainingContainerWidth / numButtons;
        var buttonHeight = tileHeight * 2;

        var buttonRowWidth = buttonWidth * numButtons + spaceBetween*(numButtons-1);
        var xo = (container.getWidth() - buttonRowWidth) * 0.5;
        for (int i = 0; i < flood.getAvailableColors().size(); i++) {
            Color availableColor = flood.getAvailableColors().get(i);
            buttons.add(new FloodButton(
                    (int) (xo + buttonWidth * i + i * spaceBetween),
                    (int) (gameHeight + (buttonAreaHeight - buttonHeight) * 0.5),
                    buttonWidth, buttonHeight, availableColor));
        }
    }

    public void mousePressed(Container container, int button, int x, int y) {
        if (flood.isFinished()) {
            flood.restart();
            return;
        }

        for (FloodButton floodButton : buttons) {
            if (floodButton.contains(x, y)) {
                flood.changeColor(floodButton.color());
            }
        }



    }

    public void render(Container container, Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, container.getWidth(), container.getHeight());

        for (int x = 0; x < flood.getWidth(); x++) {
            for (int y = 0; y < flood.getHeight(); y++) {
                g.setColor(flood.getColor(x, y));
                g.fillRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
                g.setColor(flood.getColor(x, y).darker());
                g.drawRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
            }
        }

        for (FloodButton button : buttons) {
            g.setColor(button.color());
            g.fillRect(button.x(), button.y(), button.width(), button.height());
            g.setColor(button.color().darker());
            g.drawRect(button.x(), button.y(), button.width(), button.height());
        }
    }

}
