import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    static Canvas canvas;
    static JFrame frame;
    static List<Runnable> taskQueue = new ArrayList<>();

    static Container container;
    static App app;

    static Flood flood;

    static void init() {
        container = new Container() {
            @Override
            public int getWidth() {
                return canvas.getWidth();
            }

            @Override
            public int getHeight() {
                return canvas.getHeight();
            }
        };
        flood = new Flood(10,10, Arrays.asList(
                new Color(0.8f,0.4f,0.4f),
                new Color(0.4f, 0.4f, 0.8f),
                new Color(0.4f, 0.8f, 0.4f),
                new Color(0.4f, 0.8f, 0.8f),
                new Color(0.8f, 0.8f, 0.4f),
                new Color(0.8f, 0.4f, 0.8f)
        ));
        app = new App(flood, container);
    }

    static void execOnMainThread(Runnable r) {
        synchronized (taskQueue) {
            taskQueue.add(r);
        }
    }

    public static void main(String[] args) {

        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(1024, 600));
        canvas.setIgnoreRepaint(true);
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                execOnMainThread(() -> {
                    Main.mousePressed(e.getButton(), e.getX(), e.getY());
                });
            }
        });
        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                execOnMainThread(() -> {
                    if (app!=null)app.resize();
                });
            }
        });
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(canvas);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        new Thread(Main::mainLoop).start();

    }

    static void mousePressed(int button, int x, int y) {
        app.mousePressed(container, button, x, y);
    }

    static void mainLoop() {
        init();
        while (true) {

            processTasks();
            update();
            render();

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void processTasks() {
        synchronized (taskQueue) {
            for (Runnable runnable : taskQueue) {
                runnable.run();
            }
            taskQueue.clear();
        }
    }

    static void update() {

    }

    static void render() {

        var bs = canvas.getBufferStrategy();
        if (bs == null) {
            canvas.createBufferStrategy(2);
            return;
        }

        var g = bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        app.render(container, g);

        g.dispose();
        bs.show();
    }


}