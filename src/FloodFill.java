import java.util.ArrayList;
import java.util.List;

public class FloodFill {
    private final int[] arr;
    private final int width;
    private final int height;
    private final int startValue;
    private final int startX;
    private final int startY;
    private final int newValue;

    private List<Node> open = new ArrayList<>();
    private List<Integer> closed = new ArrayList<>();

    public FloodFill(int[] arr, int width, int height, int startX, int startY, int newValue) {
        this.arr = arr;
        this.width = width;
        this.height = height;
        this.startX = startX;
        this.startY = startY;
        this.newValue = newValue;
        startValue = arr[startX + startY * width];
    }

    public void start() {
        open.add(new Node(startX, startY));
        while (!open.isEmpty()) {
            var node = open.remove(0);
            closed.add(node.x() + node.y() * width);
            fill(node);
            add(node.x() - 1, node.y());
            add(node.x() + 1, node.y());
            add(node.x(), node.y() - 1);
            add(node.x(), node.y() + 1);
        }
    }

    void add(int x, int y) {
        if (!valid(x,y)) return;
        if (arr[x + y * width] != startValue) return;
        if (closed.contains(x+y*width)) return;
        var node = new Node(x,y);
        if (open.contains(node)) return;
        open.add(node);
    }

    boolean valid(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    void fill(Node n) {
        arr[n.x() + n.y() * width] = newValue;
    }

    record Node(int x, int y) {

    }
}
