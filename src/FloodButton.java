import java.awt.*;

public record FloodButton (int x, int y, int width, int height, Color color){
    public boolean contains(int x1, int y1) {
        return x1 >= x && x1 < x + width && y1 >= y && y1 < y + height;
    }
}
